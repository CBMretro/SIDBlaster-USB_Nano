![](../images/SIDBlaster-USB_Nano-03.png)
![](../images/SIDBlaster-USB_Nano-04.png)

3D printed case for SIDBlaster-USB Nano. Print to translucent filament if you want the leds on the inside to show.

Use 3x12 screws.