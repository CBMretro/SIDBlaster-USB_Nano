# SIDBlaster-USB Nano

[![License: CERN-OHL-S-2.0](./images/licence-cern-ohl-s-2.0.svg)](https://codeberg.org/CBMretro/SIDBlaster-USB_Nano/src/branch/main/LICENSE)

![SIDBlaster-USB Nano](./images/SIDBlaster-USB_Nano-01.png "SIDBlaster-USB Nano")
![SIDBlaster-USB Nano](./images/SIDBlaster-USB_Nano-02.png "SIDBlaster-USB Nano")

This is a small playback only version of [SIDBlaster-USB Tic Tac Edition](https://github.com/gh0stless/SIDBlaster-USB-Tic-Tac-Edition) that uses only SMD components.

Stereo 3,5mm audio jack is used and mono output from SID is routed to both channels of audio jack. This makes it easy to connect SIDBlaster-USB Nano to common audio equipment with stereo 3,5mm input.

As the FT232RL USB UART IC used on the board supports USB2 only, USB-C connector is wired for USB2 so you need USB-A to USB-C cable for connecting this to PC. USB-C to USB-C cable doesn't work. At least not on my Thinkpads.

Preassembled boards can be ordered from JLCPCB. Gerber, BOM and component position file required for JLCPCB SMT assembly are in gerber directory.

I have assembled boards for sale on my [Webstore](https://cbmretro.fi/product/sidblaster-usb-nano/)

## Jumper settings

J1: Open for 12V SID 6581  
Closed for 9V SID 8580  
J2,J3: 1-2 for 6581  
J2,J3: 2-3 for 8580

## Version history

V1.3 - Soft start circuit removed as some users reported it was causing connection issues with their USB-hubs. It's not actually needed. USB specification requires soft start circuit if initial current draw of hotplugged device is over 500mA. SIDBlaster-USB Nano doesn't use that much current.

V1.2 - Filter capacitor added between USB GND and shield GND. Shorter and parallel USB datalines from USB connector to FT232RL. Noticeably lower SID background noise compared to V1.1 (and also Tic-Tac Edition) 

Better compliance to USB specification. Resettable fuse added to VBUS for over voltage and over current protection. "Soft start" circuit added to limit current draw when USB is connected.

V1.1 - Initial release.

## Case

Design files for 3D printed case are available in case directory.

![Case](./images/SIDBlaster-USB_Nano-03.png "Case")